#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

struct entry 
{
    char pass[PASS_LEN];
    char hash[HASH_LEN];
};

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char *hashguess = md5(guess, strlen(guess));
    
    // Compare the two hashes
    if (strncmp(hash, hashguess, HASH_LEN) == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    
    // Free any malloc'd memory
    free(hashguess);
}


// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.

struct entry *read_dictionary(char *filename, int *size)
{
    // Get the length
    struct stat fileinfo;
    stat(filename, &fileinfo);
    int len = fileinfo.st_size;
    
    // Allocate memory for the file
    char *file_contents = malloc(len);
    
    // Read file into file_contents
    FILE *fp = fopen(filename, "r");
    
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    
    fread(file_contents, 1, len, fp);
    
    fclose(fp);
    
    // Replace \n with \0
    int line_count = 0;
    
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    struct entry *dict = malloc(line_count * sizeof(struct entry));
    
    int c = 0;
    
    for (int i = 0; i < line_count; i++)
    {
        char *p = &file_contents[c];
        char *h = md5(p, strlen(p));
        strcpy(dict[i].pass, p);
        strcpy(dict[i].hash, h);
        free(h);
        
        while (file_contents[c] != '\0') c++;
        c++;
    }
    
    *size = line_count;
    return dict;
}

// Sort by hash
int by_hash(const void *a, const void *b)
{
    return strcmp((*(struct entry *)a).hash, (*(struct entry *)b).hash); 
}

int comp(const void *t, const void *a)
{
    return strcmp((char *)t, (*(struct entry *)a).hash);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dlen, sizeof(struct entry), by_hash);
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    FILE *h = fopen(argv[1], "r");
    if (!h)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    int crack_count = 0;
    char hash[HASH_LEN + 1];

    while (fgets(hash, HASH_LEN + 1, h) != NULL)
    {
        hash[strlen(hash) - 1] = '\0';
        struct entry * found = bsearch(hash, dict, dlen, sizeof(struct entry), comp);

        if (found != NULL)
        {
            printf("Found! %s %s\n\n", found->hash, found->pass);
            crack_count++;
        }
        else
        {
            printf("Not Found!\n\n");
        }
    }
    
    printf("Cracked: %d\n", crack_count);
}
